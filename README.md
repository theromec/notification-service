# notification-service
https://freelance.habr.com/tasks/363435
https://docs.google.com/document/d/11u9WByRDPycwpNFai78wRkyp2rqJ-1lATlMo480cO0M

# [DEVELOPMENT]

## Build dev images:
```shell
docker-compose build notification-service celery
```

## Run unittests:
```shell
docker-compose run --rm \
  -e DJANGO_SETTINGS_MODULE=notification_service.unittest \
  notification-service ./manage.py test
```

## Run service dependencies:
```shell
docker-compose up -d postgres redis maildev
```

## Run Django migrations:
```shell
docker-compose run --rm notification-service ./manage.py migrate
```

## Run API:
```shell
docker-compose up notification-service
```

## Run Celery:
```shell
docker-compose up celery
```

## Import service modules:
Use the following pattern to avoid circular imports:
```python
import api.services.message as message_service
import api.services.sender as sender_service
```

# [PRODUCTION]

## Build production image:
```shell
docker build . -t notification_service:latest
```

## Run unittests using production image:
```shell
docker run --rm \
  -e DJANGO_SETTINGS_MODULE=notification_service.unittest \
  -e DEBUG=True \
  -e SECRET_KEY=test \
  notification_service:latest python manage.py test
```

## Run Celery worker:
```shell
# 1. Run dependencies somewhere (for example using docker-compose) & update URLs in .env file:
docker-compose up -d redis postgres maildev

# 2. Run Celery worker:
docker run --rm \
  --network notification-service_default \
  --env-file=.env \
  -v celery_data:/app/celery_data \
  notification_service:latest \
  celery -A notification_service worker -l INFO --statedb=/app/celery_data/worker.state
```

## Run API:
```shell
# 1. Run dependencies somewhere (for example using docker-compose) & update URLs in .env file:
docker-compose up -d redis postgres maildev

# 2. Run Gunicorn:
docker run --rm \
  --network notification-service_default \
  --env-file=.env \
  -p 8000:8000 \
  notification_service:latest \
  gunicorn -b 0.0.0.0:8000 --access-logfile - notification_service.wsgi
```

## Run Django migrations:
```shell
docker run --rm \
  --network notification-service_default \
  --env-file=.env \
  notification_service:latest \
  python manage.py migrate
```

## Message rotation:
Run archive command in cron:
```shell
docker run --rm \
  --network notification-service_default \
  --env-file=.env \
  notification_service:latest \
  python manage.py archive
```

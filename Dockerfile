FROM python:3.9-alpine

WORKDIR /app

RUN apk update && apk add gcc musl-dev postgresql-dev

COPY requirements-freeze.txt requirements-freeze.txt

RUN pip install -r requirements-freeze.txt

RUN mkdir /app/celery_data

RUN addgroup -S user && adduser -S user -G user

RUN chown user:user /app/celery_data

COPY --chown=user:user ./src /app

USER user

from dataclasses import dataclass


@dataclass
class UpdateStateDTO:
    state: str

from dataclasses import dataclass
from typing import List

from api.dtos.sms_ru_status_log import CreateSMSRuStatusLogDTO


@dataclass
class CallbackDTO:
    data: List[CreateSMSRuStatusLogDTO]

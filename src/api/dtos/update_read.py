from dataclasses import dataclass


@dataclass
class UpdateReadDTO:
    read: str

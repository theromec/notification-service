from dataclasses import dataclass


@dataclass
class UpdateStatusDTO:
    status: str

from dataclasses import dataclass


@dataclass
class CreateSMSRuToMessageDTO:
    message_id: int
    sms_id: str
    status_code: int


@dataclass
class UpdateSMSRuToMessageDTO:
    sms_id: str
    status_code: int

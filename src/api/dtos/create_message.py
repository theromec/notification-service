from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class CreateMessageDTO:
    type: str
    email: Optional[str]
    phone: Optional[str]
    text: str
    delayed: Optional[datetime]

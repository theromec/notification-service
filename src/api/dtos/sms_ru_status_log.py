from dataclasses import dataclass


@dataclass
class CreateSMSRuStatusLogDTO:
    sms_id: str
    status_code: int
    timestamp: int

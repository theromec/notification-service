from celery import shared_task

import api.services.message as message_service


@shared_task
def send_message_task(message_id: int):
    message_service.send_message(message_id)

from rest_framework import serializers

from api.constants import READ_CHOICES
from api.dtos import UpdateReadDTO
from api.serializers.base import BaseSerializer


class ReadSerializer(BaseSerializer):
    read = serializers.ChoiceField(choices=READ_CHOICES)

    class Meta:
        dto_class = UpdateReadDTO

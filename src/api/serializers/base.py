from rest_framework import serializers


class BaseSerializer(serializers.Serializer):
    class Meta:
        dto_class = None

    def update(self, instance, validated_data: dict):
        for k, v in validated_data.items():
            setattr(instance, k, v)
        return instance

    def create(self, validated_data: dict):
        return self.Meta.dto_class(**validated_data)

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.constants import MESSAGE_TYPE_CHOICES, MESSAGE_TYPE_EMAIL, MESSAGE_TYPE_SMS
from api.dtos import CreateMessageDTO
from api.serializers.base import BaseSerializer


class SendMessageRequestSerializer(BaseSerializer):
    type = serializers.ChoiceField(choices=MESSAGE_TYPE_CHOICES)
    email = serializers.EmailField(default=None)
    phone = serializers.CharField(default=None)
    text = serializers.CharField()
    delayed = serializers.DateTimeField(required=False, default=None)

    class Meta:
        dto_class = CreateMessageDTO

    def validate(self, attrs: dict) -> dict:
        type_ = attrs['type']

        if type_ == MESSAGE_TYPE_EMAIL:
            if not attrs['email']:
                raise ValidationError('email is required.')
        elif type_ == MESSAGE_TYPE_SMS:
            if not attrs['phone']:
                raise ValidationError('phone is required.')

        return attrs


class SendMessageResponseSerializer(BaseSerializer):
    id = serializers.IntegerField()

from rest_framework import serializers

from api.serializers.base import BaseSerializer


class PauseResponseSerializer(BaseSerializer):
    on_pause = serializers.BooleanField()
    affected_messages = serializers.ListField(child=serializers.IntegerField())

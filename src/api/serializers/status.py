from rest_framework import serializers

from api.constants import STATUS_CHOICES, READ_CHOICES
from api.serializers.base import BaseSerializer


class MessageStatusSerializer(BaseSerializer):
    status = serializers.ChoiceField(choices=STATUS_CHOICES)
    read = serializers.ChoiceField(choices=READ_CHOICES)

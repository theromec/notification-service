from django.core.management.base import BaseCommand, CommandError
import api.services.message as message_service

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('--days', dest='days', type=int, default=1)

    def handle(self, *args, **options):
        message_service.archive(options['days'])

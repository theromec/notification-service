from typing import List

from django.db.transaction import atomic

import api.services.sms_ru_to_message as sms_ru_to_message_service
from api.dtos import CreateSMSRuStatusLogDTO
from api.models import SMSRuStatusLogModel


@atomic
def create(logs_dto: List[CreateSMSRuStatusLogDTO]):
    items = []
    for dto in logs_dto:
        if sms_ru_to_message_service.get_by_sms_id(dto.sms_id):
            item = SMSRuStatusLogModel(
                sms_id=dto.sms_id,
                status_code=dto.status_code,
                timestamp=dto.timestamp,
            )
            items.append(item)
    if items:
        SMSRuStatusLogModel.objects.bulk_create(items)


def get_message_id(message_id: int) -> List[SMSRuStatusLogModel]:
    sms_ru_to_message = sms_ru_to_message_service.get_by_message_id(message_id)
    if not sms_ru_to_message:
        return []
    return list(SMSRuStatusLogModel.objects.filter(sms_id=sms_ru_to_message.sms_id).order_by('timestamp'))

import logging
from datetime import timedelta
from typing import Tuple, Optional

from django.conf import settings
from django.db.transaction import atomic
from django.utils import timezone

import api.services.celery as celery_service
import api.services.pause as pause_service
import api.services.sender as sender_service
from api.constants import (
    READ_NA,
    STATUS_PAUSED,
    STATUS_QUEUED,
)
from api.dtos import CreateMessageDTO, UpdateReadDTO, UpdateStatusDTO
from api.models import MessageModel, ArchivedMessageModel

logger = logging.getLogger(settings.LOGGER_NAME)


@atomic
def create_message(i_key: Optional[str], dto: CreateMessageDTO) -> Tuple[MessageModel, bool]:
    message_model = None

    if i_key:
        message_model = MessageModel.objects.filter(i_key=i_key).first()

    if message_model:
        return message_model, False

    message_model = MessageModel.objects.create(
        i_key=i_key,
        type=dto.type,
        email=dto.email,
        phone=dto.phone,
        text=dto.text,
        delayed=dto.delayed,
        status=STATUS_PAUSED if pause_service.is_on_pause(pause_service.get_or_create_pause()) else STATUS_QUEUED,
        read=READ_NA,
    )

    return message_model, True


@atomic
def update_read(message: MessageModel, dto: UpdateReadDTO):
    if message.read != dto.read:
        message.read = dto.read
        message.save()


@atomic
def update_status(message: MessageModel, dto: UpdateStatusDTO):
    if message.status != dto.status:
        message.status = dto.status
        message.save()


def get_message_by_id(message_id: int) -> Optional[MessageModel]:
    return MessageModel.objects.filter(id=message_id).first()


def send_message(message_id: int):
    """
    Функция выполняется внутри Celery воркера. Выполняет отправку сообщения.
    """

    message: Optional[MessageModel] = get_message_by_id(message_id)

    # На случай если задача на отправку создана, но после создания сообщение было удалено из БД
    if not message:
        return

    pause = pause_service.get_or_create_pause()

    if pause_service.is_on_pause(pause):
        if message.status != STATUS_PAUSED:
            update_status(message, UpdateStatusDTO(status=STATUS_PAUSED))
        celery_service.revoke_send_message_tasks([message_id])
    else:
        if message.status == STATUS_QUEUED:
            sender = sender_service.sender_factory(message)
            sender.send()


@atomic
def archive(days: int):
    created_at__lt = timezone.now() - timedelta(days=days)
    messages = MessageModel.objects.filter(created_at__lt=created_at__lt)

    archived_messages = []

    for message in messages:
        archived_message = ArchivedMessageModel(
            message_id=message.id,
            i_key=message.i_key,
            created_at=message.created_at,
            type=message.type,
            email=message.email,
            phone=message.phone,
            text=message.text,
            delayed=message.delayed,
            status=message.status,
            read=message.read,
        )
        archived_messages.append(archived_message)

    ArchivedMessageModel.objects.bulk_create(archived_messages)

    messages.delete()

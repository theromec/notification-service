from typing import Optional

from django.db.transaction import atomic

from api.dtos import CreateSMSRuToMessageDTO, UpdateSMSRuToMessageDTO
from api.models import SMSRuToMessageModel


@atomic
def create(dto: CreateSMSRuToMessageDTO):
    SMSRuToMessageModel.objects.create(message_id=dto.message_id, sms_id=dto.sms_id, status_code=dto.status_code)


def get_by_message_id(message_id: int) -> Optional[SMSRuToMessageModel]:
    return SMSRuToMessageModel.objects.filter(message_id=message_id).first()


def get_by_sms_id(sms_id: str) -> Optional[SMSRuToMessageModel]:
    return SMSRuToMessageModel.objects.filter(sms_id=sms_id).first()


def update(dto: UpdateSMSRuToMessageDTO):
    item = get_by_sms_id(sms_id=dto.sms_id)
    if item:
        if item.status_code != dto.status_code:
            item.status_code = dto.status_code
            item.save()

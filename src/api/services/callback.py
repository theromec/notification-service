from django.db.transaction import atomic

import api.services.message as message_service
import api.services.sms_ru_status_log as sms_ru_status_log_service
import api.services.sms_ru_to_message as sms_ru_to_message_service
from api.constants import READ_YES, STATUS_ERROR, SMS_RU_ERROR_CODES, SMS_RU_READ_CODE, SMS_RU_SENT_CODE, STATUS_SENT
from api.dtos import CallbackDTO, UpdateReadDTO, UpdateSMSRuToMessageDTO, UpdateStatusDTO


@atomic
def log_sms_status(dto: CallbackDTO):
    sms_ru_status_log_service.create(dto.data)


@atomic
def update_status(dto: CallbackDTO):
    for log_dto in dto.data:
        sms_ru_to_message = sms_ru_to_message_service.get_by_sms_id(sms_id=log_dto.sms_id)

        if sms_ru_to_message:
            message = sms_ru_to_message.message

            sms_ru_to_message_service.update(
                UpdateSMSRuToMessageDTO(
                    sms_id=log_dto.sms_id,
                    status_code=log_dto.status_code,
                )
            )

            if log_dto.status_code == SMS_RU_SENT_CODE:
                message_service.update_status(message, UpdateStatusDTO(status=STATUS_SENT))

            if log_dto.status_code == SMS_RU_READ_CODE:
                message_service.update_status(message, UpdateStatusDTO(status=STATUS_SENT))
                message_service.update_read(message, UpdateReadDTO(read=READ_YES))

            if log_dto.status_code in SMS_RU_ERROR_CODES:
                message_service.update_status(message, UpdateStatusDTO(status=STATUS_ERROR))

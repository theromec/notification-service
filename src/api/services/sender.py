import logging
import time

from django.conf import settings
from django.db.transaction import atomic

import api.services.email as email_service
import api.services.message as message_service
import api.services.sms as sms_service
import api.services.sms_ru_status_log as sms_ru_status_log_service
import api.services.sms_ru_to_message as sms_ru_to_message_service
import api.services.task_to_message as task_to_message_service
from api.constants import (
    MESSAGE_TYPE_EMAIL,
    MESSAGE_TYPE_SMS,
    SEND_TASK_DONE,
    SEND_TASK_ERROR,
    STATUS_ERROR,
    STATUS_SENT,
    STATUS_PENDING,
)
from api.dtos import UpdateStateDTO, UpdateStatusDTO, CreateSMSRuToMessageDTO, CreateSMSRuStatusLogDTO
from api.models import MessageModel

logger = logging.getLogger(settings.LOGGER_NAME)


class Sender:
    response = None

    def __init__(self, message):
        self.message = message

    def _send(self):
        raise NotImplementedError

    def send(self):
        try:
            self._send()
        except Exception as e:
            self.err_callback(e)
        else:
            self.ok_callback()

    def _update_status_ok(self):
        message_service.update_status(self.message, UpdateStatusDTO(status=STATUS_SENT))

    def _update_status_err(self):
        message_service.update_status(self.message, UpdateStatusDTO(status=STATUS_ERROR))

    def _update_task_ok(self):
        tasks = task_to_message_service.get_created_tasks([self.message.id])
        task_to_message_service.set_state_task_to_message(tasks, UpdateStateDTO(state=SEND_TASK_DONE))

    def _update_task_err(self):
        tasks = task_to_message_service.get_created_tasks([self.message.id])
        task_to_message_service.set_state_task_to_message(tasks, UpdateStateDTO(state=SEND_TASK_ERROR))

    @atomic
    def ok_callback(self):
        self._update_status_ok()
        self._update_task_ok()

    @atomic
    def err_callback(self, exception: Exception):
        logger.exception(exception)
        self._update_status_err()
        self._update_task_err()


class EmailSender(Sender):
    def _send(self):
        self.response = email_service.send_email_message(self.message)


class SmsSender(Sender):
    def _send(self):
        self.response = sms_service.send_sms_message(self.message)

    def _get_sms_id_from_response(self):
        sms = self.response['sms']
        phone = list(sms.keys())[0]
        return sms[phone]['sms_id']

    def _get_status_code_from_response(self):
        sms = self.response['sms']
        phone = list(sms.keys())[0]
        return sms[phone]['status_code']

    def _update_status_ok(self):
        message_service.update_status(self.message, UpdateStatusDTO(status=STATUS_PENDING))

    def ok_callback(self):
        super(SmsSender, self).ok_callback()
        sms_ru_to_message_service.create(
            CreateSMSRuToMessageDTO(
                message_id=self.message.id,
                sms_id=self._get_sms_id_from_response(),
                status_code=self._get_status_code_from_response(),
            )
        )
        sms_ru_status_log_service.create([
            CreateSMSRuStatusLogDTO(
                sms_id=self._get_sms_id_from_response(),
                status_code=self._get_status_code_from_response(),
                timestamp=int(time.time()),
            )
        ])


def sender_factory(message: MessageModel):
    if message.type == MESSAGE_TYPE_EMAIL:
        return EmailSender(message)
    elif message.type == MESSAGE_TYPE_SMS:
        return SmsSender(message)
    else:
        raise Exception('unknown message_type')

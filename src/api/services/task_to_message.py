from typing import List

from celery.result import AsyncResult
from django.db.transaction import atomic

from api.constants import SEND_TASK_CREATED
from api.dtos import UpdateStateDTO
from api.models import TaskToMessageModel


@atomic
def create_task_to_message(message_id: int, async_result: AsyncResult):
    TaskToMessageModel.objects.create(message_id=message_id, task_id=async_result.task_id, state=SEND_TASK_CREATED)


@atomic
def set_state_task_to_message(task_to_message_list: List[TaskToMessageModel], dto: UpdateStateDTO):
    TaskToMessageModel.objects.filter(id__in=[x.id for x in task_to_message_list]).update(state=dto.state)


def get_created_tasks(message_ids: List[int]):
    return list(TaskToMessageModel.objects.filter(message_id__in=message_ids, state=SEND_TASK_CREATED))

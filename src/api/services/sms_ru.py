import logging
from urllib.parse import urlencode

import requests
from django.conf import settings

from api.constants import TEST_SMS_ID

logger = logging.getLogger(settings.LOGGER_NAME)


CLIENT = None


def get_client():
    global CLIENT
    if CLIENT:
        return CLIENT
    client_class = SMSRuClientMock if settings.SMS_RU_MOCK_CLIENT else SMSRuClient
    CLIENT = client_class(settings.SMS_RU_FROM, settings.SMS_RU_TEST, settings.SMS_RU_LOG_ENABLED)
    return CLIENT


class SMSRuClientMock:
    def __init__(self, from_: str, test_: bool, log_enabled: bool):
        self.from_ = from_
        self.test_ = test_
        self.log_enabled = log_enabled

    def send_sms(self, phone: str, text: str) -> dict:
        return {
            "status": "OK",
            "status_code": 100,
            "sms": {
                phone: {
                    "status": "OK",
                    "status_code": 100,
                    "sms_id": TEST_SMS_ID,
                    "cost": "0.00"
                }
            },
            "balance": 82.15
        }

    def setup_callback_url(self):
        pass


class SMSRuClient:
    base_url = 'https://sms.ru'

    def __init__(self, from_: str, test_: bool, log_enabled: bool):
        self.from_ = from_
        self.test_ = test_
        self.log_enabled = log_enabled

    def log(self, response):
        if self.log_enabled:
            logger.info(response.status_code)
            logger.info(response.text)

    def send_sms_build_payload(self, phone: str, text: str):
        payload = {
            'to': phone,
            'msg': text,
            'json': 1,
        }

        if self.from_:
            payload['from'] = self.from_

        if self.test_:
            payload['test'] = 1

        return payload

    def send_sms(self, phone: str, text: str) -> dict:
        """
        Response example for /sms/send:
        {
            "status": "OK",
            "status_code": 100,
            "sms": {
                "+79138423883": {
                    "status": "OK",
                    "status_code": 100,
                    "sms_id": "202118-1000000",
                    "cost": "0.00"
                }
            },
            "balance": 82.15
        }
        """
        payload = self.send_sms_build_payload(phone, text)
        params = urlencode(payload)
        response = requests.get(f'{self.base_url}/sms/send?api_id={settings.SMS_RU_API_ID}&{params}')
        self.log(response)
        return response.json()

    def setup_callback_url(self):
        """
        Response example for /callback/get:
        {
            "status": "OK", // Запрос выполнен успешно (нет ошибок в авторизации)
            "status_code": 100, // Успешный код выполнения
            "callback": [ // Список добавленных callback
                "http://yoursite.ru/callback.php", // первый сайт
                "http://anothersite.ru/callback/index.php" // второй сайт
            ]
        }
        """

        if settings.SMS_RU_CALLBACK_URL:
            response = requests.get(f'{self.base_url}/callback/get?api_id={settings.SMS_RU_API_ID}&json=1')
            self.log(response)
            body = response.json()
            if settings.SMS_RU_CALLBACK_URL not in body.get('callback', []):
                params = urlencode({
                    'url': settings.SMS_RU_CALLBACK_URL,
                    'json': 1,
                })
                response = requests.get(f'{self.base_url}/callback/add?api_id={settings.SMS_RU_API_ID}&{params}')
                self.log(response)

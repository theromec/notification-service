from typing import List

from celery.result import AsyncResult
from django.conf import settings

import api.services.message as message_service
import api.services.task_to_message as task_to_message_service
from api.constants import SEND_TASK_REVOKED
from api.dtos import UpdateStateDTO
from api.tasks import send_message_task


class ControlMock:
    def revoke(self, *args, **kwargs):
        pass


class CeleryMock:
    def __init__(self):
        self.control = ControlMock()


def get_celery_app():
    from notification_service import celery_app
    celery_app_mock = CeleryMock()
    return celery_app_mock if settings.MOCK_CELERY_APP else celery_app


def create_send_message_task(message_id: int):
    message = message_service.get_message_by_id(message_id)
    async_result: AsyncResult = send_message_task.apply_async(kwargs={'message_id': message_id}, eta=message.delayed)
    task_to_message_service.create_task_to_message(message_id, async_result)


def revoke_send_message_tasks(message_ids: List[int]):
    """
    Отменить задачу на отправку сообщения
    """
    task_to_message_list = task_to_message_service.get_created_tasks(message_ids)

    if task_to_message_list:
        get_celery_app().control.revoke([x.task_id for x in task_to_message_list])
        task_to_message_service.set_state_task_to_message(task_to_message_list, UpdateStateDTO(state=SEND_TASK_REVOKED))

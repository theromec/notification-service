from typing import List, Tuple

from django.db.transaction import atomic

from api.constants import SETTINGS_PAUSE_NAME, SETTINGS_PAUSE_ON, SETTINGS_PAUSE_OFF, STATUS_QUEUED, STATUS_PAUSED
from api.models import SettingsModel, MessageModel


@atomic
def get_or_create_pause() -> SettingsModel:
    pause = SettingsModel.objects.filter(name=SETTINGS_PAUSE_NAME).first()
    if not pause:
        pause = SettingsModel.objects.create(name=SETTINGS_PAUSE_NAME, value=SETTINGS_PAUSE_OFF)
    return pause


def is_on_pause(pause: SettingsModel) -> bool:
    return pause.value == SETTINGS_PAUSE_ON


@atomic
def set_pause_on() -> Tuple[bool, List[int]]:
    ids = []
    pause = get_or_create_pause()
    if pause.value != SETTINGS_PAUSE_ON:
        pause.value = SETTINGS_PAUSE_ON
        pause.save()
        ids = list(MessageModel.objects.filter(status=STATUS_QUEUED).values_list('id', flat=True))
        MessageModel.objects.filter(id__in=ids).update(status=STATUS_PAUSED)
    return is_on_pause(pause), ids


@atomic
def set_pause_off() -> Tuple[bool, List[int]]:
    ids = []
    pause = get_or_create_pause()
    if pause.value != SETTINGS_PAUSE_OFF:
        pause.value = SETTINGS_PAUSE_OFF
        pause.save()
        ids = list(MessageModel.objects.filter(status=STATUS_PAUSED).values_list('id', flat=True))
        MessageModel.objects.filter(id__in=ids).update(status=STATUS_QUEUED)
    return is_on_pause(pause), ids

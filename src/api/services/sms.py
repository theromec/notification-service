import api.services.sms_ru as sms_ru_service
from api.models import MessageModel


def check_response(message_id: int, response: dict):
    """
    Response OK:
    {
        "status": "OK",
        "status_code": 100,
        "sms": {
            "+71111111111": {
              "status": "OK",
              "status_code": 100,
              "sms_id": "202118-1000000",
              "cost": "0.00"
            }
        },
        "balance": 82.15
    }

    Response ERROR:

    {
        "status": "OK",
        "status_code": 100,
        "sms": {
            "+71111111111": {
                "status": "ERROR",
                "status_code": 202,
                "status_text": "Неправильно указан номер телефона получателя, либо на него нет маршрута"
            }
        },
        "balance": 82.15
    }

    OR:

    {
        "status": "ERROR",
        "status_code": 301,
        "status_text": "Неправильный api_id, либо логин\/пароль"
    }
    """

    status = response['status']

    if status == 'ERROR':
        status_code = response['status_code']
        status_text = response['status_text']
        raise Exception(f'Error while sending SMS with id {message_id}. {status_code}: {status_text}')

    sms = response['sms']
    phone = list(sms.keys())[0]

    sms_status = sms[phone]['status']

    if sms_status == 'ERROR':
        sms_status_code = sms[phone]['status_code']
        sms_status_text = sms[phone]['status_text']
        raise Exception(f'Error while sending SMS with id {message_id}. {sms_status_code}: {sms_status_text}')


def send_sms_message(sms_message: MessageModel):
    response = sms_ru_service.get_client().send_sms(sms_message.phone, sms_message.text)
    check_response(sms_message.id, response)
    return response

from django.conf import settings
from django.core.mail import send_mail

from api.models import MessageModel


def send_email_message(email_message: MessageModel):
    response = send_mail(
        subject=settings.DEFAULT_SUBJECT_EMAIL,
        message=email_message.text,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[email_message.email],
        fail_silently=False,
    )
    return response

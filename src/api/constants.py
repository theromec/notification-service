STATUS_QUEUED = 'queued'
STATUS_SENT = 'sent'
STATUS_PAUSED = 'paused'
STATUS_ERROR = 'error'
STATUS_PENDING = 'pending'
STATUS_CHOICES = (
    (STATUS_QUEUED, 'Queued',),
    (STATUS_SENT, 'Sent',),
    (STATUS_PAUSED, 'Paused',),
    (STATUS_ERROR, 'Error',),
    (STATUS_PENDING, 'Pending',),
)

READ_YES = 'yes'
READ_NO = 'no'
READ_NA = 'n/a'
READ_CHOICES = (
    (READ_YES, 'Yes',),
    (READ_NO, 'No',),
    (READ_NA, 'N/A',),
)

MESSAGE_TYPE_SMS = 'sms'
MESSAGE_TYPE_EMAIL = 'email'
MESSAGE_TYPE_CHOICES = (
    (MESSAGE_TYPE_SMS, 'Sms',),
    (MESSAGE_TYPE_EMAIL, 'Email',),
)

IDEMPOTENCY_KEY_HEADER = 'Idempotency-Key'

TEST_EMAIL = 'test@example.com'
TEST_PHONE = '+71111111111'
TEST_TEXT = 'test text'
TEST_SMS_ID = '202118-1000000'

SETTINGS_PAUSE_NAME = 'pause'
SETTINGS_PAUSE_ON = 'on'
SETTINGS_PAUSE_OFF = 'off'
SETTINGS_PAUSE_CHOICES = (
    (SETTINGS_PAUSE_ON, 'On',),
    (SETTINGS_PAUSE_OFF, 'Off',),
)

SEND_TASK_CREATED = 'created'
SEND_TASK_REVOKED = 'revoked'
SEND_TASK_DONE = 'done'
SEND_TASK_ERROR = 'error'
SEND_TASK_CHOICES = (
    (SEND_TASK_CREATED, 'Created',),
    (SEND_TASK_REVOKED, 'Revoked',),
    (SEND_TASK_DONE, 'Done',),
    (SEND_TASK_ERROR, 'Error',),
)

SMS_RU_CODES = {
    -1: 'Сообщение не найдено',
    100: 'Запрос выполнен или сообщение находится в нашей очереди',
    101: 'Сообщение передается оператору',
    102: 'Сообщение отправлено (в пути)',
    103: 'Сообщение доставлено',
    104: 'Не может быть доставлено: время жизни истекло',
    105: 'Не может быть доставлено: удалено оператором',
    106: 'Не может быть доставлено: сбой в телефоне',
    107: 'Не может быть доставлено: неизвестная причина',
    108: 'Не может быть доставлено: отклонено',
    110: 'Сообщение прочитано (для Viber, временно не работает)',
    150: 'Не может быть доставлено: не найден маршрут на данный номер',
    200: 'Неправильный api_id',
    201: 'Не хватает средств на лицевом счету',
    202: 'Неправильно указан номер телефона получателя, либо на него нет маршрута',
    203: 'Нет текста сообщения',
    204: 'Имя отправителя не согласовано с администрацией',
    205: 'Сообщение слишком длинное (превышает 8 СМС)',
    206: 'Будет превышен или уже превышен дневной лимит на отправку сообщений',
    207: 'На этот номер нет маршрута для доставки сообщений',
    208: 'Параметр time указан неправильно',
    209: 'Вы добавили этот номер (или один из номеров) в стоп-лист',
    210: 'Используется GET, где необходимо использовать POST',
    211: 'Метод не найден',
    212: 'Текст сообщения необходимо передать в кодировке UTF-8 (вы передали в другой кодировке)',
    213: 'Указано более 100 номеров в списке получателей',
    214: 'Номер находится зарубежом (включена настройка "Отправлять только на номера РФ")',
    220: 'Сервис временно недоступен, попробуйте чуть позже',
    230: 'Превышен общий лимит количества сообщений на этот номер в день',
    231: 'Превышен лимит одинаковых сообщений на этот номер в минуту',
    232: 'Превышен лимит одинаковых сообщений на этот номер в день',
    233: 'Превышен лимит отправки повторных сообщений с кодом на этот номер за короткий промежуток времени ("защита от мошенников", можно отключить в разделе "Настройки")',
    300: 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
    301: 'Неправильный api_id, либо логин/пароль',
    302: 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
    303: 'Код подтверждения неверен',
    304: 'Отправлено слишком много кодов подтверждения. Пожалуйста, повторите запрос позднее',
    305: 'Слишком много неверных вводов кода, повторите попытку позднее',
    500: 'Ошибка на сервере. Повторите запрос.',
    901: 'Callback: URL неверный (не начинается на http://)',
    902: 'Callback: Обработчик не найден (возможно был удален ранее)',
}

SMS_RU_SENT_CODE = 103

SMS_RU_READ_CODE = 110

SMS_RU_PENDING_CODES = (
    100, 101, 102,
)

SMS_RU_GOOD_CODES = (
    *SMS_RU_PENDING_CODES,
    SMS_RU_SENT_CODE,
    SMS_RU_READ_CODE,
)

SMS_RU_ERROR_CODES = (
    -1,
    104, 105, 106, 107, 108,
    150,
    200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
    210, 211, 212, 213, 214,
    220,
    230, 231, 232, 233,
    300, 301, 302, 303, 304, 305,
    500,
    901,
    902,
)

SMS_RU_RETRY_CODES = (
    201,
    220,
    500,
)

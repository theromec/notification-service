# Generated by Django 3.2 on 2021-05-10 03:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_smsrutomessagemodel'),
    ]

    operations = [
        migrations.CreateModel(
            name='SMSRuStatusLogModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('i_key', models.UUIDField(db_index=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('sms_id', models.CharField(db_index=True, max_length=255)),
                ('status', models.IntegerField(db_index=True)),
                ('timestamp', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='smsrutomessagemodel',
            name='sms_id',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]

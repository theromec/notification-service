# Generated by Django 3.2 on 2021-05-07 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_tasktomessagemodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tasktomessagemodel',
            name='state',
            field=models.CharField(choices=[('created', 'Created'), ('revoked', 'Revoked'), ('done', 'Done'), ('error', 'Error')], max_length=10),
        ),
    ]

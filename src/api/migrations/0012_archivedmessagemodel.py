# Generated by Django 3.2 on 2021-05-12 15:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_alter_messagemodel_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArchivedMessageModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('i_key', models.UUIDField(db_index=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('type', models.CharField(choices=[('sms', 'Sms'), ('email', 'Email')], max_length=10)),
                ('email', models.EmailField(max_length=254, null=True)),
                ('phone', models.CharField(max_length=15, null=True)),
                ('text', models.TextField()),
                ('delayed', models.DateTimeField(null=True)),
                ('status', models.CharField(choices=[('queued', 'Queued'), ('sent', 'Sent'), ('paused', 'Paused'), ('error', 'Error'), ('pending', 'Pending')], max_length=10)),
                ('read', models.CharField(choices=[('yes', 'Yes'), ('no', 'No'), ('n/a', 'N/A')], max_length=10)),
                ('message_id', models.BigIntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

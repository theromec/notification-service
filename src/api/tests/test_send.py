import json
import uuid
from datetime import timedelta
from unittest.mock import patch, MagicMock

from django.utils import timezone
from rest_framework.test import APITestCase

import api.services.message as message_service
from api.constants import MESSAGE_TYPE_EMAIL, STATUS_QUEUED, IDEMPOTENCY_KEY_HEADER
from api.dtos import CreateMessageDTO
from api.models import EmailMessageModel


class TestSend(APITestCase):
    @patch('api.views.send.celery_service.create_send_message_task')
    def test_post_email_message(self, send_message_mock: MagicMock):
        message = {
            'type': MESSAGE_TYPE_EMAIL,
            'email': 'test@example.com',
            'text': 'Test message text.',
        }
        headers = {
            IDEMPOTENCY_KEY_HEADER: str(uuid.uuid4())
        }
        response = self.client.post('/send/', json.dumps(message), content_type='application/json', **headers)
        self.assertEqual(response.status_code, 201)
        send_message_mock.assert_called_once()
        message_id = response.data['id']
        self.assertEqual(STATUS_QUEUED, EmailMessageModel.objects.get(id=message_id).status)

    @patch('api.views.send.celery_service.create_send_message_task')
    def test_post_email_message_already_created(self, send_message_mock: MagicMock):
        i_key = str(uuid.uuid4())
        message = {
            'type': MESSAGE_TYPE_EMAIL,
            'email': 'test@example.com',
            'text': 'Test message text.',
        }
        headers = {
            IDEMPOTENCY_KEY_HEADER: i_key
        }
        dto = CreateMessageDTO(
            type=message['type'],
            email=message['email'],
            phone=None,
            text=message['text'],
            delayed=None,
        )
        message_service.create_message(i_key, dto)
        response = self.client.post('/send/', json.dumps(message), content_type='application/json', **headers)
        self.assertEqual(response.status_code, 200)
        send_message_mock.assert_not_called()
        message_id = response.data['id']
        self.assertEqual(STATUS_QUEUED, EmailMessageModel.objects.get(id=message_id).status)

    @patch('api.views.send.celery_service.create_send_message_task')
    def test_post_email_message_no_i_key(self, send_message_mock: MagicMock):
        message = {
            'type': MESSAGE_TYPE_EMAIL,
            'email': 'test@example.com',
            'text': 'Test message text.',
        }
        response = self.client.post('/send/', json.dumps(message), content_type='application/json')
        self.assertEqual(response.status_code, 201)
        send_message_mock.assert_called_once()
        message_id = response.data['id']
        self.assertEqual(STATUS_QUEUED, EmailMessageModel.objects.get(id=message_id).status)

    @patch('api.views.send.celery_service.create_send_message_task')
    def test_post_delayed_email_message(self, send_message_mock: MagicMock):
        message = {
            'type': MESSAGE_TYPE_EMAIL,
            'email': 'test@example.com',
            'text': 'Test message text.',
            'delayed': (timezone.now() + timedelta(days=1)).isoformat()
        }
        headers = {
            IDEMPOTENCY_KEY_HEADER: str(uuid.uuid4())
        }
        response = self.client.post('/send/', json.dumps(message), content_type='application/json', **headers)
        self.assertEqual(response.status_code, 201)
        send_message_mock.assert_called_once()
        message_id = response.data['id']
        self.assertEqual(STATUS_QUEUED, EmailMessageModel.objects.get(id=message_id).status)

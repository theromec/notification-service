from datetime import timedelta
from unittest.mock import patch, MagicMock

from django.test import TestCase
from django.utils import timezone

import api.services.message as message_service
import api.services.pause as pause_service
from api.constants import STATUS_SENT, STATUS_ERROR, STATUS_PAUSED
from api.models import ArchivedMessageModel, MessageModel
from api.tests.base import create_test_email_message, create_test_sms_message


class TestMessageService(TestCase):
    def test_send_message_ok(self):
        message = create_test_email_message()
        message_service.send_message(message.id)
        message.refresh_from_db()
        self.assertEqual(STATUS_SENT, message.status)

    @patch('api.services.sender.email_service.send_email_message')
    def test_send_message_err(self, send_mail_mock: MagicMock):
        send_mail_mock.side_effect = Exception('test error')
        message = create_test_email_message()
        message_service.send_message(message.id)
        message.refresh_from_db()
        self.assertEqual(STATUS_ERROR, message.status)

    def test_send_message_pause(self):
        pause_service.set_pause_on()
        message = create_test_email_message()
        message_service.send_message(message.id)
        message.refresh_from_db()
        self.assertEqual(STATUS_PAUSED, message.status)

    def test_archive(self):
        email = create_test_email_message()
        sms = create_test_sms_message()

        email.created_at = timezone.now() - timedelta(days=31)
        email.save()

        sms.created_at = timezone.now() - timedelta(days=31)
        sms.save()

        message_service.archive(30)

        self.assertTrue(ArchivedMessageModel.objects.filter(message_id=email.id).exists())
        self.assertTrue(ArchivedMessageModel.objects.filter(message_id=sms.id).exists())
        self.assertFalse(MessageModel.objects.filter(id=email.id).exists())
        self.assertFalse(MessageModel.objects.filter(id=sms.id).exists())

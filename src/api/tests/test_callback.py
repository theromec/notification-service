from rest_framework.test import APITestCase

import api.services.sender as sender_service
from api.constants import (
    TEST_SMS_ID,
    STATUS_PENDING,
    READ_YES,
    STATUS_ERROR,
    STATUS_SENT,
    SMS_RU_READ_CODE,
    SMS_RU_SENT_CODE, READ_NA,
)
from api.tests.base import create_test_sms_message


class TestCallback(APITestCase):
    def test_sms_status_pending(self):
        message = create_test_sms_message()
        sender_service.SmsSender(message).send()
        payload = {
            'data[0]': f'sms_status\n{TEST_SMS_ID}\n{102}\n1620618949',
            'hash': '000'
        }
        self.client.post('/callback/', payload)
        message.refresh_from_db()
        self.assertEqual(STATUS_PENDING, message.status)
        self.assertEqual(READ_NA, message.read)

    def test_sms_status_read(self):
        message = create_test_sms_message()
        sender_service.SmsSender(message).send()
        payload = {
            'data[0]': f'sms_status\n{TEST_SMS_ID}\n{SMS_RU_READ_CODE}\n1620618949',
            'hash': '000'
        }
        self.client.post('/callback/', payload)
        message.refresh_from_db()
        self.assertEqual(STATUS_SENT, message.status)
        self.assertEqual(READ_YES, message.read)

    def test_sms_status_error(self):
        message = create_test_sms_message()
        sender_service.SmsSender(message).send()
        payload = {
            'data[0]': f'sms_status\n{TEST_SMS_ID}\n{104}\n1620618949',
            'hash': '000'
        }
        self.client.post('/callback/', payload)
        message.refresh_from_db()
        self.assertEqual(STATUS_ERROR, message.status)
        self.assertEqual(READ_NA, message.read)

    def test_sms_status_sent(self):
        message = create_test_sms_message()
        sender_service.SmsSender(message).send()
        payload = {
            'data[0]': f'sms_status\n{TEST_SMS_ID}\n{SMS_RU_SENT_CODE}\n1620618949',
            'hash': '000'
        }
        self.client.post('/callback/', payload)
        message.refresh_from_db()
        self.assertEqual(STATUS_SENT, message.status)
        self.assertEqual(READ_NA, message.read)

    def test_unknown_sms_id(self):
        message = create_test_sms_message()
        sender_service.SmsSender(message).send()
        payload = {
            'data[0]': f'sms_status\n202118-1000001\n{SMS_RU_SENT_CODE}\n1620618949',
            'hash': '000'
        }
        self.client.post('/callback/', payload)
        message.refresh_from_db()
        self.assertEqual(STATUS_PENDING, message.status)
        self.assertEqual(READ_NA, message.read)

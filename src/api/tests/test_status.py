from rest_framework.test import APITestCase

from api.constants import STATUS_QUEUED, READ_NA
from api.tests.base import create_test_email_message


class TestStatus(APITestCase):
    def test_email_message_status(self):
        message = create_test_email_message()
        response = self.client.get(f'/status/{message.id}/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(STATUS_QUEUED, response.data['status'])
        self.assertEqual(READ_NA, response.data['read'])

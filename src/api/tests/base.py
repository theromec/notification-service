import json
import uuid

import api.services.message as message_service
from api.constants import MESSAGE_TYPE_EMAIL, MESSAGE_TYPE_SMS, TEST_EMAIL, TEST_TEXT, TEST_PHONE
from api.dtos import CreateMessageDTO
from api.models import MessageModel


def create_test_email_message() -> MessageModel:
    i_key = str(uuid.uuid4())
    dto = CreateMessageDTO(
        type=MESSAGE_TYPE_EMAIL,
        email=TEST_EMAIL,
        phone=None,
        text=TEST_TEXT,
        delayed=None,
    )
    message, _ = message_service.create_message(i_key, dto)
    return message


def create_test_sms_message():
    i_key = str(uuid.uuid4())
    dto = CreateMessageDTO(
        type=MESSAGE_TYPE_SMS,
        email=None,
        phone=TEST_PHONE,
        text=TEST_TEXT,
        delayed=None,
    )
    message, _ = message_service.create_message(i_key, dto)
    return message


def create_test_email_message_via_api(api_client) -> int:
    message = {
        'type': MESSAGE_TYPE_EMAIL,
        'email': TEST_EMAIL,
        'text': TEST_TEXT,
    }
    return api_client.post('/send/', json.dumps(message), content_type='application/json').data['id']


def create_test_sms_message_via_api(api_client) -> int:
    message = {
        'type': MESSAGE_TYPE_SMS,
        'phone': TEST_PHONE,
        'text': TEST_TEXT,
    }
    return api_client.post('/send/', json.dumps(message), content_type='application/json').data['id']

import uuid
from unittest.mock import patch, MagicMock

from celery.result import AsyncResult
from django.test import TestCase

import api.services.celery as celery_service
import api.services.task_to_message as task_to_message_service
from api.tests.base import create_test_email_message


class TestCeleryService(TestCase):
    @patch('api.services.celery.send_message_task.apply_async')
    def test_create_send_message_task(self, apply_async_mock: MagicMock):
        apply_async_mock.return_value = AsyncResult(id=str(uuid.uuid4()))
        message = create_test_email_message()
        celery_service.create_send_message_task(message.id)
        self.assertEqual(1, len(task_to_message_service.get_created_tasks([message.id])))

from unittest.mock import patch

from rest_framework.test import APITestCase

from api.constants import SEND_TASK_CREATED, SEND_TASK_REVOKED
from api.models import TaskToMessageModel
from api.tests.base import create_test_email_message_via_api


class TestPause(APITestCase):
    @patch('api.tasks.message_service.send_message', lambda *_, **__: None)
    def test_set_pause_on(self):
        message_id = create_test_email_message_via_api(self.client)
        response = self.client.post('/pause/')  # set pause on
        self.assertEqual(200, response.status_code)
        self.assertEqual(True, response.data['on_pause'])
        self.assertEqual([message_id], response.data['affected_messages'])
        self.assertEqual(SEND_TASK_REVOKED, TaskToMessageModel.objects.get(message_id=message_id).state)

    @patch('api.tasks.message_service.send_message', lambda *_, **__: None)
    def test_set_pause_off(self):
        message_id = create_test_email_message_via_api(self.client)
        response = self.client.post('/pause/')  # set pause on
        response = self.client.post('/pause/')  # set pause off
        self.assertEqual(200, response.status_code)
        self.assertEqual(False, response.data['on_pause'])
        self.assertEqual([message_id], response.data['affected_messages'])
        self.assertEqual(SEND_TASK_REVOKED, TaskToMessageModel.objects.filter(message_id=message_id).order_by('id')[0].state)
        self.assertEqual(SEND_TASK_CREATED, TaskToMessageModel.objects.filter(message_id=message_id).order_by('id')[1].state)

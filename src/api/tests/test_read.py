import json

from rest_framework.test import APITestCase

from api.constants import READ_YES
from api.tests.base import create_test_email_message


class TestRead(APITestCase):
    def test_read(self):
        message = create_test_email_message()
        response = self.client.patch(f'/read/{message.id}/', json.dumps({'read': READ_YES}), content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(READ_YES, response.data['read'])

from unittest.mock import patch

from django.test import TestCase

import api.services.sender as sender_service
import api.services.sms_ru_status_log as sms_ru_status_log_service
from api.constants import STATUS_SENT, STATUS_ERROR, TEST_PHONE, STATUS_PENDING
from api.tests.base import create_test_sms_message, create_test_email_message


class TestSenderService(TestCase):
    def test_send_sms_status_ok(self):
        message = create_test_sms_message()
        sender = sender_service.SmsSender(message)
        sender.send()
        message.refresh_from_db()
        self.assertEqual(STATUS_PENDING, message.status)
        sms_ru_logs = sms_ru_status_log_service.get_message_id(message.id)
        self.assertEqual(1, len(sms_ru_logs))
        self.assertEqual(100, sms_ru_logs[0].status_code)

    def test_send_sms_status_err(self):
        message = create_test_sms_message()
        sender = sender_service.SmsSender(message)
        with patch('api.services.sms_ru.SMSRuClientMock.send_sms') as send_sms_mock:
            send_sms_mock.return_value = {
                'status': 'OK',
                'status_code': 100,
                'sms': {
                    TEST_PHONE: {
                        'status': 'ERROR',
                        'status_code': 202,
                        'status_text': 'Неправильно указан номер телефона получателя, либо на него нет маршрута'
                    }
                },
                'balance': 82.15
            }
            sender.send()
        message.refresh_from_db()
        self.assertEqual(STATUS_ERROR, message.status)
        sms_ru_logs = sms_ru_status_log_service.get_message_id(message.id)
        self.assertEqual(0, len(sms_ru_logs))

    def test_send_sms_exception_err(self):
        message = create_test_sms_message()
        sender = sender_service.SmsSender(message)
        with patch('api.services.sender.SmsSender._send') as send_mock:
            send_mock.side_effect = Exception('http error')
            sender.send()
        message.refresh_from_db()
        self.assertEqual(STATUS_ERROR, message.status)
        sms_ru_logs = sms_ru_status_log_service.get_message_id(message.id)
        self.assertEqual(0, len(sms_ru_logs))

    def test_send_email_status_ok(self):
        message = create_test_email_message()
        sender = sender_service.EmailSender(message)
        sender.send()
        message.refresh_from_db()
        self.assertEqual(STATUS_SENT, message.status)

    def test_send_email_exception_err(self):
        message = create_test_email_message()
        sender = sender_service.EmailSender(message)
        with patch('api.services.sender.EmailSender._send') as send_mock:
            send_mock.side_effect = Exception('smtp error')
            sender.send()
        message.refresh_from_db()
        self.assertEqual(STATUS_ERROR, message.status)

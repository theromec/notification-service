from django.apps import AppConfig

import api.services.sms_ru as sms_ru_service


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'

    def ready(self):
        sms_ru_service.get_client().setup_callback_url()

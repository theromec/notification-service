from django.db import models


class BaseModel(models.Model):
    i_key = models.UUIDField(db_index=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

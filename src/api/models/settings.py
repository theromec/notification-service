from django.db import models

from api.models.base import BaseModel


class SettingsModel(BaseModel):
    name = models.CharField(max_length=255, unique=True)
    value = models.CharField(max_length=1000)

    def __str__(self):
        return f'{self.name}:{self.value}'

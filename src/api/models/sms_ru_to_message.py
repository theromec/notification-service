from django.db import models

from api.models.message import MessageModel
from api.models.base import BaseModel


class SMSRuToMessageModel(BaseModel):
    message = models.ForeignKey(MessageModel, on_delete=models.CASCADE)
    sms_id = models.CharField(max_length=255, unique=True)
    status_code = models.IntegerField()

    def __str__(self):
        return f'{self.message_id}:{self.sms_id}'

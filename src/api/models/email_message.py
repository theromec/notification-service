from django.db import models

from api.constants import MESSAGE_TYPE_EMAIL
from api.models.message import MessageModel


class EmailMessageModelManager(models.Manager):
    def get_queryset(self):
        return super(EmailMessageModelManager, self).get_queryset().filter(type=MESSAGE_TYPE_EMAIL)


class EmailMessageModel(MessageModel):
    objects = EmailMessageModelManager()
    class Meta:
        proxy = True

from django.db import models

from api.constants import SEND_TASK_CHOICES
from api.models.message import MessageModel
from api.models.base import BaseModel


class TaskToMessageModel(BaseModel):
    task_id = models.CharField(max_length=36)
    message = models.ForeignKey(MessageModel, on_delete=models.CASCADE)
    state = models.CharField(max_length=10, choices=SEND_TASK_CHOICES)

    def __str__(self):
        return f'{self.message_id}:{self.state}'

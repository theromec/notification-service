from django.db import models

from api.models.message_base import MessageBaseModel


class ArchivedMessageModel(MessageBaseModel):
    message_id = models.BigIntegerField()

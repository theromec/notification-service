from django.db import models

from api.models.base import BaseModel
from api.models.sms_ru_to_message import SMSRuToMessageModel


class SMSRuStatusLogModel(BaseModel):
    sms = models.ForeignKey(SMSRuToMessageModel, to_field='sms_id', on_delete=models.CASCADE)
    status_code = models.IntegerField(db_index=True)
    timestamp = models.IntegerField()

    def __str__(self):
        return f'{self.sms_id}:{self.status_code}'

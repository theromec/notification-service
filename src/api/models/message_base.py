from django.db import models

from api.constants import STATUS_CHOICES, MESSAGE_TYPE_CHOICES, READ_CHOICES, MESSAGE_TYPE_EMAIL
from api.models.base import BaseModel


class MessageBaseModel(BaseModel):
    type = models.CharField(max_length=10, choices=MESSAGE_TYPE_CHOICES)
    email = models.EmailField(null=True)
    phone = models.CharField(max_length=15, null=True)
    text = models.TextField()
    delayed = models.DateTimeField(null=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES)
    read = models.CharField(max_length=10, choices=READ_CHOICES)

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self.id}:{self.type}:{self.status}:{self.email if self.type == MESSAGE_TYPE_EMAIL else self.phone}:{self.text}:{self.delayed}'

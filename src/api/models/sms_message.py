from django.db import models

from api.constants import MESSAGE_TYPE_SMS
from api.models.message import MessageModel


class SmsMessageModelManager(models.Manager):
    def get_queryset(self):
        return super(SmsMessageModelManager, self).get_queryset().filter(type=MESSAGE_TYPE_SMS)


class SmsMessageModel(MessageModel):
    objects = SmsMessageModelManager()
    class Meta:
        proxy = True

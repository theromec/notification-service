import logging

from django.conf import settings
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import MultiPartParser
from rest_framework.request import Request
from rest_framework.response import Response

import api.services.callback as callback_service
from api.dtos import CallbackDTO, CreateSMSRuStatusLogDTO

logger = logging.getLogger(settings.LOGGER_NAME)


@parser_classes([MultiPartParser])
@api_view(['POST'])
def callback(request: Request):
    log_dtos = []
    for key, value in request.data.items():
        if str(key).startswith('data') and str(value).startswith('sms_status'):
            sms_status_data = value.split('\n')
            sms_id = sms_status_data[1]
            status_code = int(sms_status_data[2])
            timestamp = int(sms_status_data[3])
            log_dto = CreateSMSRuStatusLogDTO(sms_id=sms_id, status_code=status_code, timestamp=timestamp)
            log_dtos.append(log_dto)
    callback_service.log_sms_status(CallbackDTO(data=log_dtos))
    callback_service.update_status(CallbackDTO(data=log_dtos))
    return Response('100', status=200)

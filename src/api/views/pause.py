from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response

import api.services.celery as celery_service
import api.services.pause as pause_service
from api.serializers.base import BaseSerializer
from api.serializers.pause import PauseResponseSerializer
from api.views.base import BaseViewSet


class PauseViewSet(BaseViewSet):
    serializer_class = BaseSerializer

    @extend_schema(
        request=None,
        responses=PauseResponseSerializer,
    )
    def create(self, request, *_, **__):
        pause = pause_service.get_or_create_pause()

        if pause_service.is_on_pause(pause):
            on_pause, message_ids = pause_service.set_pause_off()
            for message_id in message_ids:
                celery_service.create_send_message_task(message_id)
        else:
            on_pause, message_ids = pause_service.set_pause_on()
            celery_service.revoke_send_message_tasks(message_ids)

        response = {
            'on_pause': on_pause,
            'affected_messages': message_ids,
        }

        return Response(PauseResponseSerializer(response).data, status=status.HTTP_200_OK)

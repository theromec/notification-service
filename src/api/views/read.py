from rest_framework import status
from rest_framework.response import Response

import api.services.message as message_service
from api.serializers.read import ReadSerializer
from api.views.base import BaseViewSet


class ReadViewSet(BaseViewSet):
    serializer_class = ReadSerializer

    def partial_update(self, request, *args, **kwargs):
        message = message_service.get_message_by_id(kwargs['pk'])

        if not message:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer: ReadSerializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        dto = serializer.save()
        message_service.update_read(message, dto)

        return Response(ReadSerializer(message).data, status=status.HTTP_200_OK)

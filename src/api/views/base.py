from typing import Optional

from rest_framework import viewsets

from api.constants import IDEMPOTENCY_KEY_HEADER


class BaseViewSet(viewsets.GenericViewSet):
    def get_i_key(self) -> Optional[str]:
        return self.request.META.get(IDEMPOTENCY_KEY_HEADER)

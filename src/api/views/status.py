from rest_framework import mixins

from api.models import MessageModel
from api.serializers.status import MessageStatusSerializer
from api.views.base import BaseViewSet


class StatusViewSet(mixins.RetrieveModelMixin, BaseViewSet):
    queryset = MessageModel.objects.all()
    serializer_class = MessageStatusSerializer

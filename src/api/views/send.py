from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import status
from rest_framework.response import Response

import api.services.celery as celery_service
import api.services.message as message_service
import api.services.pause as pause_service
from api.constants import IDEMPOTENCY_KEY_HEADER
from api.serializers.send import (
    SendMessageRequestSerializer,
    SendMessageResponseSerializer,
)
from api.views.base import BaseViewSet


class SendViewSet(BaseViewSet):
    serializer_class = SendMessageRequestSerializer

    @extend_schema(
        parameters=[OpenApiParameter(
            name=IDEMPOTENCY_KEY_HEADER,
            type=str,
            location=OpenApiParameter.HEADER,
            required=False,
            description='Ключ идемпотентности. Позволяет безопасно ретраить запросы.'
        )],
        request=SendMessageRequestSerializer,
        responses=SendMessageResponseSerializer,
    )
    def create(self, request, *_, **__):
        serializer: SendMessageRequestSerializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        dto = serializer.save()

        message_model, created = message_service.create_message(self.get_i_key(), dto)

        if created and not pause_service.is_on_pause(pause_service.get_or_create_pause()):
            celery_service.create_send_message_task(message_model.id)

        return Response(
            SendMessageResponseSerializer(message_model).data,
            status=status.HTTP_201_CREATED if created else status.HTTP_200_OK
        )

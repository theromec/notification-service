import os
from pathlib import Path

import django12factor

custom_settings = (
    'CELERY_BROKER_URL',
    'SMS_RU_API_ID',
    'SMS_RU_FROM',
    'SMS_RU_CALLBACK_URL',
)
d12f = django12factor.factorise(custom_settings=custom_settings)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

DEBUG = d12f['DEBUG']
LOGGING = d12f['LOGGING']
DATABASES = d12f['DATABASES']
ALLOWED_HOSTS = d12f['ALLOWED_HOSTS']
SECRET_KEY = d12f['SECRET_KEY']
CACHES = d12f['CACHES']


LOGGER_NAME = 'notification_service'


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'drf_spectacular',

    'api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'notification_service.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'notification_service.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'


# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
}

if not DEBUG:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
        'rest_framework.renderers.JSONRenderer',
    )


MOCK_CELERY_APP = False
CELERY_BROKER_URL = d12f['CELERY_BROKER_URL']
# CELERY_TASK_ALWAYS_EAGER = True


EMAIL_BACKEND = d12f['EMAIL_BACKEND']
EMAIL_HOST = d12f['EMAIL_HOST']
EMAIL_PORT = d12f['EMAIL_PORT']
EMAIL_HOST_USER = d12f['EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = d12f['EMAIL_HOST_PASSWORD']
EMAIL_USE_TLS = d12f['EMAIL_USE_TLS']


DEFAULT_FROM_EMAIL = 'noreply@example.com'
DEFAULT_SUBJECT_EMAIL = 'Notification'


SMS_RU_API_ID = d12f['SMS_RU_API_ID']
SMS_RU_FROM = d12f['SMS_RU_FROM']
SMS_RU_TEST = django12factor.getenv_bool('SMS_RU_TEST')
SMS_RU_MOCK_CLIENT = False
SMS_RU_LOG_ENABLED = True
SMS_RU_CALLBACK_URL = d12f['SMS_RU_CALLBACK_URL']

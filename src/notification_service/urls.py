from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework import routers

from api.views.callback import callback
from api.views.pause import PauseViewSet
from api.views.read import ReadViewSet
from api.views.send import SendViewSet
from api.views.status import StatusViewSet

router = routers.DefaultRouter()
router.register(r'pause', PauseViewSet, basename='pause')
router.register(r'read', ReadViewSet, basename='read')
router.register(r'send', SendViewSet, basename='send')
router.register(r'status', StatusViewSet, basename='status')

urlpatterns = [
    path('', RedirectView.as_view(url='/api/schema/swagger-ui/')),
    path('', include(router.urls)),
    path('callback/', callback),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns += [
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    path('api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
]

from .settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

MOCK_CELERY_APP = True
CELERY_BROKER_URL = ''
CELERY_TASK_ALWAYS_EAGER = True


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


SMS_RU_MOCK_CLIENT = True
